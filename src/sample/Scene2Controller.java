package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Scene2Controller {

    @FXML
    Label fname;

    @FXML
    Label lname;

    @FXML
    Label agelbl;

    @FXML
    Label citylbl;

    @FXML
    Label niclbl;

    @FXML
    Label vaclbl;

    @FXML
    Label boothlbl;

    @FXML
    Label datelbl;


    public void displayName(String username,String lastname,String age,String city,String nic,String vac,String boothnum,String date) {
        fname.setText(username);
        lname.setText(lastname);
        agelbl.setText(age);
        citylbl.setText(city);
        niclbl.setText(nic);
        vaclbl.setText(vac);
        boothlbl.setText(boothnum);
        datelbl.setText(date);

    }
}