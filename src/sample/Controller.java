package sample;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Controller {

    @FXML
    TextField fnameTextField;

    @FXML
    TextField lnameTextField;

    @FXML
    TextField ageTextField;

    @FXML
    TextField cityTextField;

    @FXML
    TextField nicTextField;

    @FXML
    private Pane titlePane;

    @FXML
    private Button closeButton;

    @FXML
    private Button minButton;

    @FXML
    private RadioButton rbtn1, rbtn2, rbtn3;

    @FXML
    private Label vactype;

    @FXML
    private Label booth;

    private Stage stage;
    private Scene scene;
    private Parent root;

    public void receipt(ActionEvent event) throws IOException {

        String username = fnameTextField.getText();
        String lastname = lnameTextField.getText();
        String age = ageTextField.getText();
        String city = cityTextField.getText();
        String nic = nicTextField.getText();
        String vac = vactype.getText();
        String boothnum = booth.getText();


        Date date1 = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String date = formatter.format(date1);
        date = formatter.format(date1);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("Scene2.fxml"));
        root = loader.load();

        Scene2Controller scene2Controller = loader.getController();
        scene2Controller.displayName(username, lastname, age, city, nic,vac,boothnum,date);

        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    private double x, y;

    public void init(Stage stage) {
        titlePane.setOnMousePressed(mouseEvent -> {
            x = mouseEvent.getSceneX();
            y = mouseEvent.getSceneY();
        });
        titlePane.setOnMouseDragged(mouseEvent -> {
            stage.setX(mouseEvent.getScreenX() - x);
            stage.setY(mouseEvent.getScreenY() - y);
        });

        closeButton.setOnMouseClicked(mouseEvent -> stage.close());
        minButton.setOnMouseClicked(mouseEvent -> stage.setIconified(true));
    }

    public void vacBill(ActionEvent event) {

        if (rbtn1.isSelected()) {
            vactype.setText(rbtn1.getText());
            booth.setText("0 or 1");
        } else if (rbtn2.isSelected()) {
            vactype.setText(rbtn2.getText());
            booth.setText("2 or 3");
        } else if (rbtn3.isSelected()) {
            vactype.setText(rbtn3.getText());
            booth.setText("4 or 5");
        }
    }
}